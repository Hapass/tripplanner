﻿using System.Data.Entity;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.DataAccess.EntityDatabase
{
    public class TripplannerContext : DbContext
    {
        public DbSet<Airport> Airports { get; set; }

        public TripplannerContext() : base("TripplannerConnectionString")
        {
        }
    }
}
