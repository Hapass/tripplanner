﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain;

namespace Tripplanner.DataAccess.EntityDatabase
{ 
    public class EntityRepository<TEntity> : IEntityRepository<TEntity> where TEntity : Entity
    {
        protected DbContext DbContext { get; private set; }

        public EntityRepository(DbContext context)
        {
            DbContext = context;
        }

        public virtual TEntity GetById(int id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> query)
        {
            return DbContext.Set<TEntity>().Where(query);
        }

        public virtual void Add(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
        }

        public void Update(TEntity entity)
        {
            DbContext.Set<TEntity>().Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
        }

        public void SaveChanges()
        {
            DbContext.SaveChanges();
        }
    }
}
