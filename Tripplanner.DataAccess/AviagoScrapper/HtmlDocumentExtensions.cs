using System.IO;
using System.Text;
using HtmlAgilityPack;

namespace Tripplanner.DataAccess.AviagoScrapper
{
    internal static class HtmlDocumentExtensions
    {
        public static HtmlDocument LoadDocument(this HtmlDocument document, StreamReader streamReader)
        {
            document.Load(streamReader);
            return document;
        }

        public static HtmlDocument ToHtmlDocument(this string text)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(text);
            return doc;
        }
    }
}