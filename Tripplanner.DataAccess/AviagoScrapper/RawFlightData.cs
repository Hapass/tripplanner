using System;
using System.Collections.Generic;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.DataAccess.AviagoScrapper
{
    internal class RawFlightData
    {
        public FlightRequest FlightRequest { get; set; }
        public List<decimal> PricesCorrespondingToOptions { get; set; }
        public List<string> AirlinesCorrespondingToOptions { get; set; }
        public List<DateTime> DepartureTimesCorrespondingToOptions { get; set; }
        public List<TimeSpan> TimesInFlightCorrespondingToOptions { get; set; }
        public List<int> LaggageWeightsCorrespondingToOptions { get; set; }
    }
}