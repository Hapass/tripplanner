namespace Tripplanner.DataAccess.AviagoScrapper
{
    internal static class UnicodeConstants
    {
        public const string EuroSign = "\u20AC";
        public const string RussianMinutes = "\u043C\u0438\u043D";
        public const string RussianHours = "\u0447";
        public const string RussianWeightPerPerson = "\u043A\u0433/\u0447\u0435\u043B.";
        public const string Multiply = "x";
    }
}