﻿/* We are sorry aviago.by, that we are scrapping data from your site, but your site doesn't provide any terms of use,
 * and therefore it is legal to use your publicly accessible data for scrapping.
 * We do not intend to resell your data or make harm to your business in any way,
 * so we will give a link to your site wherever some information we got from it appears in our application.
 * If you have any problems with us scrapping your data, please clarify the terms of use of your website and then contact us via bitbucket.
 */

using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using AutoMapper;
using HtmlAgilityPack;
using Tripplanner.DataAccess.HttpWrappers;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.DataAccess.AviagoScrapper
{
    public class AviagoScrapper : IFlightDataGetter
    {
        private readonly IHttpRequestFactory httpRequestFactory;
        private readonly IMappingEngine mappingEngine;

        private const string AviagoResultsPageRequestFormat = @"https://aviago.by/search_results/{0}-{1}/{2}/?adults=1&cabin=none&airline=all&currency=EUR&direct=on";
        private const string AviagoFlightApiRequestFormat = @"https://aviago.by/api/search.php?id2=search_results&adults=1&cabin=none&airline=all&currency=EUR&direct=on&session=&iata[1]={0}&iata[2]={1}&depDate={2}&journey=oneway&id=flights&aviago=true&process_offers=/var/www/aviago.ru/includes/sr/process_offers.php&providers[0][0]=epower&providers[0][1]=lt&providers[1][0]=waavo";
        private const string AviagoFlightOptionsRequest = @"https://aviago.by/includes/sr/offers.php?lng=ru&rid=&session=&last=true&lng=lt";

        private readonly CookieContainer cookieContainer;

        private string aviagoResultsPageRequest;
        private string aviagoFlightApiRequest;

        /// <summary>
        /// Delay in milliseconds.
        /// Can't use the callback, because we let aviago to use it.
        /// Default value is 5000 milliseconds.
        /// </summary>
        public int IataApiDelay { get; set; }

        public AviagoScrapper(IHttpRequestFactory httpRequestFactory, IMappingEngine mappingEngine)
        {
            this.httpRequestFactory = httpRequestFactory;
            this.mappingEngine = mappingEngine;
            this.cookieContainer = new CookieContainer();
            this.IataApiDelay = 5000;
        }

        public IList<Flight> GetFlightInformation(FlightRequest flightRequest)
        {
            aviagoResultsPageRequest = GetResultsPageRequest(flightRequest);
            aviagoFlightApiRequest = GetFlightApiRequest(flightRequest);
            RequestSessionForGivenSearch();
            CallFlightApiWithSessionAndWait();
            return mappingEngine
                .Map<List<Flight>>(new AviagoFlightHtmlParser(GetHtmlWithFlightInformation(), flightRequest)
                .Parse());
        }

        private string GetResultsPageRequest(FlightRequest flightRequest)
        {
            return string.Format(AviagoResultsPageRequestFormat,
                flightRequest.FromAirpotCode,
                flightRequest.ToAirportCode,
                flightRequest.DepartureDate.ToString("yyyy-MM-dd"));
        }

        private string GetFlightApiRequest(FlightRequest flightRequest)
        {
            return string.Format(
                AviagoFlightApiRequestFormat,
                flightRequest.FromAirpotCode,
                flightRequest.ToAirportCode,
                flightRequest.DepartureDate.ToString("yyyy-MM-dd"));
        }

        private void RequestSessionForGivenSearch()
        {
            httpRequestFactory.Create(aviagoResultsPageRequest)
                .AddCommonHeaders()
                .AddAcceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
                .AddCacheControlHeader("max-age=0")
                .AddCookieContainer(cookieContainer)
                .GetResponse()
                .Close();
        }

        private void CallFlightApiWithSessionAndWait()
        {
            httpRequestFactory.Create(aviagoFlightApiRequest)
                .AddCommonHeaders()
                .AddKeepAliveHeaders()
                .AddAcceptHeader("text/event-stream")
                .AddCacheControlHeader("no-cache")
                .AddReferer(aviagoResultsPageRequest)
                .AddCookieContainer(cookieContainer)
                .GetResponse()
                .Close();

            //Wait for IATA to handle the request
            Thread.Sleep(IataApiDelay);
        }

        private HtmlDocument GetHtmlWithFlightInformation()
        {
            using (IHttpWebResponseWrapper response = GetResponseFromFlightApi())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                return new HtmlDocument().LoadDocument(reader);
            }
        }

        private IHttpWebResponseWrapper GetResponseFromFlightApi()
        {
            return httpRequestFactory.Create(AviagoFlightOptionsRequest)
                .AddCommonHeaders()
                .AddUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36")
                .AddKeepAliveHeaders()
                .AddAcceptHeader("text/html, */*; q=0.01")
                .AddReferer(aviagoResultsPageRequest)
                .AddCookieContainer(cookieContainer)
                .AddRequestedWithAjaxHeader()
                .SetupAutomaticGzipDecompression()
                .GetResponse();
        }
    }
}
