using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using HtmlAgilityPack;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.DataAccess.AviagoScrapper
{
    internal class AviagoFlightHtmlParser
    {
        private const string SelectFlightTimeSpanNodes = @"//td[@width='176']//div[@class='offerTime']";
        private const string SelectPriceNodes = @"//div[@class='offerPrice']";
        private const string SelectAllFlightsForGivenPrice = @"//div[@class='offerPrice' and contains(.,'{0}')]/ancestor::div[@class='white_block']//div[contains(@class,'segment')]";
        private const string SelectAirlineNodes = @"//div[contains(@class,'segment')]//img[@title]";
        private const string SelectLaggageWeights = @"//td[@class='small']/strong";
        private const string SelectDepartureTimes = @"(//div[@class='offerTime'])[position() mod 3 = 1]";
        private const string AirlineNameAttribute = @"title";

        private readonly HtmlDocument flightHtmlDocument;
        private readonly FlightRequest flightRequest;
        private readonly string[] possibleTimespanFormats;

        public AviagoFlightHtmlParser(HtmlDocument flightHtmlDocument, FlightRequest flightRequest)
        {
            this.flightHtmlDocument = flightHtmlDocument;
            this.flightRequest = flightRequest;
            this.possibleTimespanFormats = new[]
                {
                    "H " + UnicodeConstants.RussianHours + " m " + UnicodeConstants.RussianMinutes,
                    "H " + UnicodeConstants.RussianHours + " mm " + UnicodeConstants.RussianMinutes,
                    "HH " + UnicodeConstants.RussianHours + " mm " + UnicodeConstants.RussianMinutes,
                    "HH " + UnicodeConstants.RussianHours + " m " + UnicodeConstants.RussianMinutes,
                    "m " + UnicodeConstants.RussianMinutes,
                    "mm " + UnicodeConstants.RussianMinutes
                };
        }

        public RawFlightData Parse()
        {
            var data = new RawFlightData
            {
                FlightRequest = flightRequest,
                PricesCorrespondingToOptions = GetPriceInformation(),
                AirlinesCorrespondingToOptions = GetAirlineInformation(),
                DepartureTimesCorrespondingToOptions = GetDepartureTimeInformation(),
                TimesInFlightCorrespondingToOptions = GetTimeInFlightInformation(),
                LaggageWeightsCorrespondingToOptions = GetLaggageWeightPerPerson()
            };
            return data;
        }

        private List<decimal> GetPriceInformation()
        {
            List<decimal> priceInformationList = new List<decimal>();
            flightHtmlDocument.DocumentNode.SelectNodes(SelectPriceNodes)
                .Select(priceNode => priceNode.InnerText.Trim())
                .Distinct().ToList()
                .ForEach(rawPriceString => priceInformationList.AddRange(GetSamePriceTimesFlightOptions(rawPriceString)));
            return priceInformationList;
        }

        private IEnumerable<decimal> GetSamePriceTimesFlightOptions(string rawPriceString)
        {
            return Enumerable.Range(0, flightHtmlDocument.DocumentNode
                .SelectNodes(GetFlightsForGivenPriceQuery(rawPriceString)).Count)
                .Select(x => ParseRawPriceString(rawPriceString));
        }

        private string GetFlightsForGivenPriceQuery(string rawPriceString)
        {
            return string.Format(SelectAllFlightsForGivenPrice, rawPriceString);
        }

        private decimal ParseRawPriceString(string rawPriceString)
        {
            int priceEndsIndex = rawPriceString.IndexOf(UnicodeConstants.EuroSign, StringComparison.OrdinalIgnoreCase);
            return decimal.Parse(rawPriceString.Substring(0, priceEndsIndex).Trim(),
                NumberStyles.Currency,
                CultureInfo.GetCultureInfo("ru-RU"));
        }

        private List<string> GetAirlineInformation()
        {
            return flightHtmlDocument.DocumentNode.SelectNodes(SelectAirlineNodes)
                .Select(x => x.GetAttributeValue(AirlineNameAttribute, string.Empty)).ToList();
        }

        private List<DateTime> GetDepartureTimeInformation()
        {
            return flightHtmlDocument.DocumentNode.SelectNodes(SelectDepartureTimes)
                .Select(x => flightRequest.DepartureDate.Add(TimeSpan.Parse(x.InnerText.Trim())))
                .ToList();
        }

        private List<TimeSpan> GetTimeInFlightInformation()
        {
            return flightHtmlDocument.DocumentNode.SelectNodes(SelectFlightTimeSpanNodes)
                .Select(x => DateTime.ParseExact(x.InnerText.Trim(), possibleTimespanFormats,
                    CultureInfo.InvariantCulture, DateTimeStyles.None).TimeOfDay).ToList();
        }

        private List<int> GetLaggageWeightPerPerson()
        {
            return flightHtmlDocument.DocumentNode.SelectNodes(SelectLaggageWeights)
                .Select(x => ParseRawWeightString(x.InnerText.Trim())).ToList();
        }

        private int ParseRawWeightString(string rawWeightString)
        {
            int weightNumberBegingAt = GetWeightNumberStringBeginning(rawWeightString);
            int weightNumberEndsAt = GetWeightNumberStringEnd(rawWeightString);
            return Int32.Parse(rawWeightString.Substring(weightNumberBegingAt, weightNumberEndsAt - weightNumberBegingAt).Trim());
        }

        private int GetWeightNumberStringEnd(string rawWeightString)
        {
            return rawWeightString.IndexOf(UnicodeConstants.RussianWeightPerPerson, StringComparison.OrdinalIgnoreCase);
        }

        private int GetWeightNumberStringBeginning(string rawWeightString)
        {
            return rawWeightString.IndexOf(UnicodeConstants.Multiply, StringComparison.OrdinalIgnoreCase) + 1;
        }
    }
}