﻿using System;

namespace Tripplanner.DataAccess.WikitravelScrapper
{
    public static class StringExtentions
    {
        public static string RemoveEverythingAbove(this string text, string substring)
        {
            int erasingLength = text.IndexOf(substring);
            if (erasingLength < 0)
            {
                throw new ArgumentException("Substring wasn't found");
            }

            return text.Substring(erasingLength, text.Length - erasingLength);
        }

        public static string RemoveEverythingBelow(this string text, string substring)
        {
            int removalPos = text.IndexOf(substring) + substring.Length - 1;
            if (removalPos < 0)
            {
                throw new ArgumentException("Substring wasn't found");
            }

            return text.Substring(0, removalPos);
        }
    }
}
