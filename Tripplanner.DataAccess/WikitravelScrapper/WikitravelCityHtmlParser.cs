﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Tripplanner.DataAccess.AviagoScrapper;
using Tripplanner.Logic.Domain.Cities;

namespace Tripplanner.DataAccess.WikitravelScrapper
{
    internal class WikitravelCityHtmlParser
    {
        private const string SelectAttractions = "//p/child::b[1] | //li/child::b[1] | //a/child::b[1] | //span[@class=\"fn org\"]";
        private const string UpperBound = "id=\"See\"";
        private const string BottomBound = "<h2>";

        private readonly string cityHtmlPage;
        private readonly string city;

        public WikitravelCityHtmlParser(string cityHtmlPage, string city)
        {
            this.cityHtmlPage = cityHtmlPage;
            this.city = city;
        }

        public City Parse()
        {
            var data = new City
            {
                Name = city,
                Attractions = GetAttractionsInformation()
            };
            return data;
        }

        private List<string> GetAttractionsInformation()
        {
            return GetReducedHtmlDocument().DocumentNode
                .SelectNodes(SelectAttractions)
                .Select(x => x.InnerText)
                .Select(a => Regex.Replace(a, @"\(.+?\)", string.Empty).Trim())
                .ToList();
        }

        private HtmlDocument GetReducedHtmlDocument()
        {
            return cityHtmlPage
                .RemoveEverythingAbove(UpperBound)
                .RemoveEverythingBelow(BottomBound)
                .ToHtmlDocument();
        }
    }
}
