﻿using System.IO;
using Tripplanner.DataAccess.HttpWrappers;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Cities;

namespace Tripplanner.DataAccess.WikitravelScrapper
{
    public class WikitravelScrapper : ICityDataGetter
    {
        private readonly IHttpRequestFactory httpRequestFactory;

        private const string PageRequestFormat = @"http://wikitravel.org/en/{0}";
        private string pageRequest;
        
        public WikitravelScrapper(IHttpRequestFactory httpRequestFactory)
        {
            this.httpRequestFactory = httpRequestFactory;
        }

        public City GetCityInformation(string city)
        {
            pageRequest = GetPageRequest(city);
            var parser = new WikitravelCityHtmlParser(GetHtmlWithCityInformation(), city);
            return parser.Parse();
        }

        private string GetPageRequest(string city)
        {
            return string.Format(PageRequestFormat, city);
        }

        private string GetHtmlWithCityInformation()
        {
            using (IHttpWebResponseWrapper response = GetResponse())
            using (TextReader reader = new StreamReader(response.GetResponseStream()))
            {
                return reader.ReadToEnd();
            }
        }

        private IHttpWebResponseWrapper GetResponse()
        {
            return httpRequestFactory.Create(pageRequest)
                .AddCommonHeaders()
                .AddKeepAliveHeaders()
                .AddAcceptHeader("*/*")
                .AddRequestedWithAjaxHeader()
                .SetupAutomaticGzipDecompression()
                .AddCacheControlHeader("max-age=0")
                .AddUserAgent("TripplannerBot/1.0 (alckevich@live.com)")
                .GetResponse();
        }
    }
}
