using System.Data.Entity.Migrations;

namespace Tripplanner.DataAccess.Migrations
{
    public partial class AddedFirstAirports : DbMigration
    {
        public override void Up()
        {
            Sql("set IDENTITY_INSERT dbo.Airports on");
            Sql("insert into dbo.Airports (Id, AirportCode, TimeZoneId, City, Country, FriendlyName) values (1, 'MSQ', 'E. Europe Standard Time', 'Minsk', 'Belarus', 'Minsk National Airport')");
            Sql("insert into dbo.Airports (Id, AirportCode, TimeZoneId, City, Country, FriendlyName) values (2, 'RIX', 'FLE Standard Time', 'Riga', 'Latvia', 'Riga International Airport')");
            Sql("insert into dbo.Airports (Id, AirportCode, TimeZoneId, City, Country, FriendlyName) values (3, 'VNO', 'FLE Standard Time', 'Vilnius', 'Lithuania', 'Vilnius Airport')");
            Sql("insert into dbo.Airports (Id, AirportCode, TimeZoneId, City, Country, FriendlyName) values (4, 'WAW', 'Central European Standard Time', 'Warsaw', 'Poland', 'Warsaw Chopin Airport')");
            Sql("insert into dbo.Airports (Id, AirportCode, TimeZoneId, City, Country, FriendlyName) values (5, 'LED', 'Russian Standard Time', 'Saint Petersburg', 'Russia', 'Pulkovo International Airport')");
            Sql("insert into dbo.Airports (Id, AirportCode, TimeZoneId, City, Country, FriendlyName) values (6, 'DME', 'Russian Standard Time', 'Moscow', 'Russia', 'Domodedovo International Airport')");
            Sql("set IDENTITY_INSERT dbo.Airports off");
        }
        
        public override void Down()
        {
            Sql("delete from dbo.Airports where Id=6");
            Sql("delete from dbo.Airports where Id=5");
            Sql("delete from dbo.Airports where Id=4");
            Sql("delete from dbo.Airports where Id=3");
            Sql("delete from dbo.Airports where Id=2");
            Sql("delete from dbo.Airports where Id=1");
        }
    }
}
