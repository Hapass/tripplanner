using System.Data.Entity.Migrations;

namespace Tripplanner.DataAccess.Migrations
{
    public partial class AddedAirportEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Airports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AirportCode = c.String(),
                        TimeZoneId = c.String(),
                        City = c.String(),
                        Country = c.String(),
                        FriendlyName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Airports");
        }
    }
}
