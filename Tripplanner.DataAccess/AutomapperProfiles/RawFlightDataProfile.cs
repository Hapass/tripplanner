﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AutoMapper;
using Tripplanner.DataAccess.AviagoScrapper;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.DataAccess.AutomapperProfiles
{
    public class RawFlightDataProfile : Profile
    {
        private readonly IEntityRepository<Airport> airportRepository;

        public RawFlightDataProfile(IEntityRepository<Airport> airportRepository)
        {
            this.airportRepository = airportRepository;
        }

        protected override void Configure()
        {
            CreateMap<RawFlightData, List<Flight>>().ConvertUsing(rawFlightData =>
                Enumerable.Range(0, GetRawFlightDataOptionCount(rawFlightData))
                .Select(optionNumber => GetFlightFromRawData(rawFlightData, optionNumber))
                .ToList());
        }

        private int GetRawFlightDataOptionCount(RawFlightData rawFlightData)
        {
            Debug.Assert(rawFlightData.AirlinesCorrespondingToOptions.Count == rawFlightData.DepartureTimesCorrespondingToOptions.Count &&
                         rawFlightData.AirlinesCorrespondingToOptions.Count == rawFlightData.LaggageWeightsCorrespondingToOptions.Count &&
                         rawFlightData.AirlinesCorrespondingToOptions.Count == rawFlightData.PricesCorrespondingToOptions.Count &&
                         rawFlightData.AirlinesCorrespondingToOptions.Count == rawFlightData.TimesInFlightCorrespondingToOptions.Count);
            return rawFlightData.AirlinesCorrespondingToOptions.Count;
        }

        private Flight GetFlightFromRawData(RawFlightData rawFlightData, int optionNumber)
        {
            return new FlightBuilder(airportRepository, rawFlightData, optionNumber).BuildAirline()
                    .BuildArrivalAirportCode().BuildArrivalTime().BuildCost()
                    .BuildDepartureAirportCode().BuildDepartureTime().BuildLaggageWeightPerPerson()
                    .BuildTimeInFlight().GetFlight();
        }
    }
}
