﻿using System;
using System.Linq;
using Tripplanner.DataAccess.AviagoScrapper;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.DataAccess.AutomapperProfiles
{
    internal class FlightBuilder
    {
        private readonly RawFlightData rawFlightData;
        private readonly int optionNumber;
        private readonly Flight flight;
        private readonly Airport departureAirport;

        public FlightBuilder(IEntityRepository<Airport> airportRepository, RawFlightData rawFlightData, int optionNumber)
        {
            this.rawFlightData = rawFlightData;
            this.optionNumber = optionNumber;
            this.departureAirport = airportRepository.Get(x => x.AirportCode == rawFlightData.FlightRequest.FromAirpotCode).First();
            this.flight = new Flight();
        }

        public FlightBuilder BuildAirline()
        {
            flight.Airline = rawFlightData.AirlinesCorrespondingToOptions[optionNumber];
            return this;
        }

        public FlightBuilder BuildArrivalAirportCode()
        {
            flight.ArrivalAirportCode = rawFlightData.FlightRequest.ToAirportCode;
            return this;
        }

        public FlightBuilder BuildDepartureTime()
        {
            flight.DepartureTime = GetDepartureTime();
            return this;
        }

        public FlightBuilder BuildArrivalTime()
        {
            flight.ArrivalTime = GetDepartureTime().Add(rawFlightData.TimesInFlightCorrespondingToOptions[optionNumber]);
            return this;
        }

        private DateTimeOffset GetDepartureTime()
        {
            TimeZoneInfo info = TimeZoneInfo.FindSystemTimeZoneById(departureAirport.TimeZoneId);
            DateTime dateTime = rawFlightData.DepartureTimesCorrespondingToOptions[optionNumber];
            return new DateTimeOffset(dateTime, info.GetUtcOffset(dateTime));
        }

        public FlightBuilder BuildCost()
        {
            flight.Cost = rawFlightData.PricesCorrespondingToOptions[optionNumber];
            return this;
        }

        public FlightBuilder BuildDepartureAirportCode()
        {
            flight.DepartureAirportCode = rawFlightData.FlightRequest.FromAirpotCode;
            return this;
        }

        public FlightBuilder BuildLaggageWeightPerPerson()
        {
            flight.LaggageWeightPerPerson = rawFlightData.LaggageWeightsCorrespondingToOptions[optionNumber];
            return this;
        }

        public FlightBuilder BuildTimeInFlight()
        {
            flight.TimeInFlight = rawFlightData.TimesInFlightCorrespondingToOptions[optionNumber];
            return this;
        }

        public Flight GetFlight()
        {
            return flight;
        }
    }
}