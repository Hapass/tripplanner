﻿using System.Net;
using Tripplanner.DataAccess.HttpWrappers;

namespace Tripplanner.DataAccess.Fakes
{
    public class FakeHttpRequestWrapper : IHttpWebRequestWrapper
    {
        private readonly FakeHttpResponseWrapper wrapper;

        public FakeHttpRequestWrapper(FakeHttpResponseWrapper wrapper)
        {
            this.wrapper = wrapper;
        }

        public IHttpWebResponseWrapper GetResponse()
        {
            return wrapper;
        }

        public IHttpWebRequestWrapper AddCommonHeaders()
        {
            return this;
        }

        public IHttpWebRequestWrapper AddKeepAliveHeaders()
        {
            return this;
        }

        public IHttpWebRequestWrapper AddReferer(string refererUrl)
        {
            return this;
        }

        public IHttpWebRequestWrapper AddCacheControlHeader(string value)
        {
            return this;
        }

        public IHttpWebRequestWrapper AddCookieContainer(CookieContainer container)
        {
            return this;
        }

        public IHttpWebRequestWrapper AddAcceptHeader(string value)
        {
            return this;
        }

        public IHttpWebRequestWrapper AddRequestedWithAjaxHeader()
        {
            return this;
        }

        public IHttpWebRequestWrapper SetupAutomaticGzipDecompression()
        {
            return this;
        }

        public IHttpWebRequestWrapper AddUserAgent(string value)
        {
            return this;
        }
    }
}
