﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.DataAccess.Fakes
{
    internal class FakeDatabaseInitializer : DropCreateDatabaseAlways<FakeTripplannerContext>
    {
        protected override void Seed(FakeTripplannerContext context)
        {
            context.Airports.AddOrUpdate(new Airport
            {
                AirportCode = "LED",
                City = "Saint Petersburg",
                Country = "Russia",
                FriendlyName = "Pulkovo Airport",
                TimeZoneId = "E. Europe Standard Time"
            });

            context.Airports.AddOrUpdate(new Airport
            {
                AirportCode = "DME",
                City = "Moscow",
                Country = "Russia",
                FriendlyName = "Domodedovo International Airport",
                TimeZoneId = "E. Europe Standard Time"
            });

            context.Airports.AddOrUpdate(new Airport
            {
                AirportCode = "MSQ",
                City = "Minsk",
                Country = "Belarus",
                FriendlyName = "Minsk National Airport",
                TimeZoneId = "E. Europe Standard Time"
            });
        }
    }
}