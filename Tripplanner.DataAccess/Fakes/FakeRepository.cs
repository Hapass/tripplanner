﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain;

namespace Tripplanner.DataAccess.Fakes
{
    public class FakeRepository<TEntity> : IEntityRepository<TEntity> where TEntity : Entity
    {
        private readonly List<TEntity> entities;

        public FakeRepository()
        {
            entities = new List<TEntity>();
        }  

        public TEntity GetById(int id)
        {
            return entities.Find(e => e.Id == id);
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> query)
        {
            return entities.FindAll(x => query.Compile()(x));
        }

        public void Add(TEntity entity)
        {
            entities.Add(entity);
        }

        public void Update(TEntity entity)
        {
            TEntity found = GetById(entity.Id);
            entities.Remove(found);
            entities.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            entities.Remove(entity);
        }

        public void SaveChanges()
        {}
    }
}
