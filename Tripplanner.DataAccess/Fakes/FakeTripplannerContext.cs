﻿using System.Data.Entity;
using Tripplanner.DataAccess.EntityDatabase;

namespace Tripplanner.DataAccess.Fakes
{
    public class FakeTripplannerContext: TripplannerContext
    {
        public FakeTripplannerContext()
        {
            Database.SetInitializer(new FakeDatabaseInitializer());
        }
    }
}
