﻿using Tripplanner.DataAccess.HttpWrappers;

namespace Tripplanner.DataAccess.Fakes
{
    public class FakeHttpRequestFactory : IHttpRequestFactory
    {
        private readonly FakeHttpRequestWrapper wrapper;

        public FakeHttpRequestFactory(FakeHttpRequestWrapper wrapper)
        {
            this.wrapper = wrapper;
        }

        public IHttpWebRequestWrapper Create(string url)
        {
            return wrapper;
        }
    }
}
