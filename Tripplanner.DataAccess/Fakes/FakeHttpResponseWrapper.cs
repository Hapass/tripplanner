﻿using System;
using System.IO;
using System.Reflection;
using Tripplanner.DataAccess.HttpWrappers;

namespace Tripplanner.DataAccess.Fakes
{
    public class FakeHttpResponseWrapper : IHttpWebResponseWrapper
    {
        private readonly string fileName;

        public FakeHttpResponseWrapper(string fileName)
        {
            this.fileName = fileName;
        }

        public void Close()
        {}

        public Stream GetResponseStream()
        {
            return Assembly.Load(new AssemblyName("Tripplanner.Tests")).GetManifestResourceStream("Tripplanner.Tests.TestData." + fileName);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {}
    }
}
