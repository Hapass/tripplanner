﻿using System.Net;

namespace Tripplanner.DataAccess.HttpWrappers
{
    public class HttpWebRequestWrapper : IHttpWebRequestWrapper
    {
        private readonly HttpWebRequest request;

        public HttpWebRequestWrapper(string url)
        {
            request = (HttpWebRequest) WebRequest.Create(url);
        }

        public IHttpWebResponseWrapper GetResponse()
        {
            return new HttpWebResponseWrapper(request.GetResponse());
        }

        public IHttpWebRequestWrapper AddCommonHeaders()
        {
            request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
            request.Headers.Add("Accept-Language", "en-US,en;q=0.8");
            return this;
        }

        public IHttpWebRequestWrapper AddKeepAliveHeaders()
        {
            request.ProtocolVersion = HttpVersion.Version10;
            request.KeepAlive = true;
            return this;
        }

        public IHttpWebRequestWrapper AddReferer(string refererUrl)
        {
            request.Referer = refererUrl;
            return this;
        }

        public IHttpWebRequestWrapper AddCacheControlHeader(string value)
        {
            request.Headers.Add("Cache-Control", value);
            return this;
        }

        public IHttpWebRequestWrapper AddCookieContainer(CookieContainer container)
        {
            request.CookieContainer = container;
            return this;
        }

        public IHttpWebRequestWrapper AddAcceptHeader(string value)
        {
            request.Accept = value;
            return this;
        }

        public IHttpWebRequestWrapper AddRequestedWithAjaxHeader()
        {
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");
            return this;
        }

        public IHttpWebRequestWrapper SetupAutomaticGzipDecompression()
        {
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            return this;
        }

        public IHttpWebRequestWrapper AddUserAgent(string value)
        {
            request.UserAgent = value;
            return this;
        }
    }
}
