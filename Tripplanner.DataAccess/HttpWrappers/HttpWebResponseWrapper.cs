﻿using System;
using System.IO;
using System.Net;

namespace Tripplanner.DataAccess.HttpWrappers
{
    public class HttpWebResponseWrapper : IHttpWebResponseWrapper
    {
        private bool disposed = false;
        private readonly HttpWebResponse response;

        public HttpWebResponseWrapper(WebResponse response)
        {
            this.response = (HttpWebResponse)response;
        }

        public void Close()
        {
            response.Close();
        }

        public Stream GetResponseStream()
        {
            return response.GetResponseStream();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            } 

            if (disposing)
            {
                response.Dispose();
            }

            disposed = true;
        }
    }
}
