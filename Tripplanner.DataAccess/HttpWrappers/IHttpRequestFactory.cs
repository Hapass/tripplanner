﻿namespace Tripplanner.DataAccess.HttpWrappers
{
    public interface IHttpRequestFactory
    {
        IHttpWebRequestWrapper Create(string url);
    }
}
