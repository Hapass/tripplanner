﻿using System;
using System.IO;

namespace Tripplanner.DataAccess.HttpWrappers
{
    public interface IHttpWebResponseWrapper : IDisposable
    {
        void Close();

        Stream GetResponseStream();
    }
}
