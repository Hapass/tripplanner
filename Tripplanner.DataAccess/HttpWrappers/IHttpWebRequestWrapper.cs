﻿using System.Net;

namespace Tripplanner.DataAccess.HttpWrappers
{
    public interface IHttpWebRequestWrapper
    {
        IHttpWebResponseWrapper GetResponse();

        IHttpWebRequestWrapper AddCommonHeaders();

        IHttpWebRequestWrapper AddKeepAliveHeaders();

        IHttpWebRequestWrapper AddReferer(string refererUrl);

        IHttpWebRequestWrapper AddCacheControlHeader(string value);

        IHttpWebRequestWrapper AddCookieContainer(CookieContainer container);

        IHttpWebRequestWrapper AddAcceptHeader(string value);

        IHttpWebRequestWrapper AddRequestedWithAjaxHeader();

        IHttpWebRequestWrapper SetupAutomaticGzipDecompression();

        IHttpWebRequestWrapper AddUserAgent(string value);
    }
}
