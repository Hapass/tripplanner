﻿namespace Tripplanner.DataAccess.HttpWrappers
{
    public class HttpRequestFactory : IHttpRequestFactory
    {
        public IHttpWebRequestWrapper Create(string url)
        {
            return new HttpWebRequestWrapper(url);
        }
    }
}
