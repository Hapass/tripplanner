﻿///<reference path="../components/angular.js"/>
///<reference path="../components/angular-mocks.js"/>
///<reference path="../components/jasmine.js"/>

///<reference path="../app/route-selector.js"/>

describe("route-selector", function () {

    beforeEach(module("routeSelector"));

    describe("route requirements controller", function () {
        var requirementsController, $scope, $httpBackend, tripDataService;

        beforeEach(inject(function ($controller, _$location_, _$rootScope_, _tripplannerService_, _tripDataService_, _$httpBackend_) {
            $scope = _$rootScope_.$new();
            $httpBackend = _$httpBackend_;

            $httpBackend.expectGET("/Airport/SupportedCities").respond({
                data: ["MinskAirport"]
            });

            tripDataService = _tripDataService_;

            requirementsController = $controller("RouteRequirementsController", {
                $scope: $scope,
                $location: _$location_,
                tripplannerService: _tripplannerService_,
                tripDataService: _tripDataService_
            });
        }));

        it("should be initialized properly.", function () {
            expect($scope.model).not.toBeNull();
            expect($scope.cities).not.toBeNull();
            expect($scope.isSubmitting).toBeUndefined();
            expect($scope.isLoading).toBeUndefined();

            $httpBackend.flush();

            expect($scope.cities).toEqual(jasmine.arrayContaining(["MinskAirport"]));
        });

        it("should reset properties on $routeChangeSuccess.", function () {
            $httpBackend.flush();
            $scope.$broadcast("$routeChangeSuccess");
            expect($scope.isLoading).toEqual(false);
        });

        it("should not submit data to server if form is not valid.", function () {
            $httpBackend.flush();
            $scope.tripRequirements = {
                $invalid: true
            }
            $scope.planTrip({ destinationPoint: "Moscow" });
            expect($scope.isLoading).toBeUndefined();
        });

        it("should submit data to server if form is valid.", function () {
            $httpBackend.flush();

            $httpBackend.expectPOST("/Trip/Find").respond({
                data: {
                    airline: "Belavia"
                },
                success: true
            });

            $scope.tripRequirements = {
                $invalid: false
            }

            $scope.planTrip({ destinationPoint: "Moscow" });

            expect($scope.isLoading).toEqual(true);

            $httpBackend.flush();

            expect(tripDataService.getData().airline).toEqual("Belavia");
        });

        it("should not save any data if server returnes error.", function () {
            $httpBackend.flush();

            $httpBackend.expectPOST("/Trip/Find").respond({
                success: false
            });

            $scope.tripRequirements = {
                $invalid: false
            }

            $scope.planTrip({ destinationPoint: "Moscow" });

            expect($scope.isLoading).toEqual(true);

            $httpBackend.flush();

            expect(tripDataService.getData().airline).toBeUndefined();
        });
    });

    describe("route results controller", function() {
        var requirementsController, $scope;

        beforeEach(inject(function ($controller, _$rootScope_, tripDataService) {
            $scope = _$rootScope_.$new();

            tripDataService.saveData({airline:"Belavia"});

            requirementsController = $controller("RouteResultsController", {
                $scope: $scope,
                tripDataService: tripDataService
            });
        }));

        it("should assign data from trip data service to the model.", function () {
            expect($scope.model.airline).toEqual("Belavia");
        });
    });
});