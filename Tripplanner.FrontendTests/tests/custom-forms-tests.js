﻿///<reference path="../components/angular.js"/>
///<reference path="../components/angular-mocks.js"/>
///<reference path="../components/jasmine.js"/>

///<reference path="../app/custom-forms.js"/>

describe("custom-forms", function() {

    beforeEach(module("customForms"));

    describe("xss validator", function() {
        var $compile, $rootScope, xssValidator;

        beforeEach(inject(function(_$compile_, _$rootScope_) {
            $compile = _$compile_;
            $rootScope = _$rootScope_;

            $compile("<form name='formName'><input ng-model='modelName' name='inputName' xss required></form>")($rootScope);
            xssValidator = $rootScope.formName.inputName.$validators.xss;
        }));

        it("should add validator to ngModelController.", function () {
            expect(xssValidator).not.toBeNull();
        });

        it("should mark input as invalid if text contains dangerous characters.", function () {
            expect(xssValidator("<html></html>", "<html></html>")).toBe(false);
        });

        it("should mark input as valid if the text is an empty string.", function () {
            expect(xssValidator("", "")).toBe(true);
        });

        it("should mark input as valid if the text does not contain dangerous characters.", function () {
            expect(xssValidator("Minsk", "Minsk")).toBe(true);
        });
    });
});