﻿using System.Data.Entity;
using Tripplanner.DataAccess.Fakes;
using Tripplanner.DataAccess.HttpWrappers;

namespace Tripplanner.ServiceBindings.Tests
{
    public class IntegrationTestsBindings : ServiceBindingsBase
    {
        public override void Load()
        {
            Bind<DbContext>().ToMethod(x => new FakeTripplannerContext()).InSingletonScope();
            Bind<IHttpRequestFactory>().To<HttpRequestFactory>();
            base.Load();
        }
    }
}
