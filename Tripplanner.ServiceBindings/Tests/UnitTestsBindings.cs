﻿using Ninject;
using Tripplanner.DataAccess.AviagoScrapper;
using Tripplanner.DataAccess.Fakes;
using Tripplanner.DataAccess.HttpWrappers;
using Tripplanner.DataAccess.WikitravelScrapper;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.ServiceBindings.Tests
{
    public class UnitTestsBindings : ServiceBindingsBase
    {
        private readonly string fileName;

        public UnitTestsBindings(string fileName = null)
        {
            this.fileName = fileName;
        }

        public override void Load()
        {
            Bind<FakeHttpResponseWrapper>().ToMethod(x => new FakeHttpResponseWrapper(fileName));
            Bind<IHttpRequestFactory>().To<FakeHttpRequestFactory>();

            base.Load();
        }

        protected override void BindRepositories()
        {
            Bind<IEntityRepository<Airport>>().ToConstant(GetAirportRepository());
        }

        protected override void BindRemoteServices()
        {
            Bind<IFlightDataGetter>().To<AviagoScrapper>().InSingletonScope();

            AviagoScrapper scrapper = KernelInstance.Get<IFlightDataGetter>() as AviagoScrapper;
            if (scrapper != null)
            {
                scrapper.IataApiDelay = 0;
            }

            Bind<ICityDataGetter>().To<WikitravelScrapper>();
        }

        private FakeRepository<Airport> GetAirportRepository()
        {
            FakeRepository<Airport> fakeRepository = new FakeRepository<Airport>();
            fakeRepository.Add(new Airport
            {
                AirportCode = "MSQ",
                City = "Minsk",
                Country = "Belarus",
                FriendlyName = "Minsk National Airport",
                TimeZoneId = "E. Europe Standard Time"
            });
            fakeRepository.Add(new Airport
            {
                AirportCode = "LED",
                City = "Saint Petersburg",
                Country = "Russia",
                FriendlyName = "Pulkovo Airport",
                TimeZoneId = "E. Europe Standard Time"
            });
            return fakeRepository;
        }
    }
}
