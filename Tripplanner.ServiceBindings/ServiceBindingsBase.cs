﻿using AutoMapper;
using AutoMapper.Impl;
using AutoMapper.Internal;
using AutoMapper.Mappers;
using Ninject;
using Ninject.Modules;
using Tripplanner.DataAccess.AutomapperProfiles;
using Tripplanner.DataAccess.AviagoScrapper;
using Tripplanner.DataAccess.EntityDatabase;
using Tripplanner.DataAccess.WikitravelScrapper;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;
using Tripplanner.Logic.ServiceInterfaces;
using Tripplanner.Logic.Services;

namespace Tripplanner.ServiceBindings
{
    public class ServiceBindingsBase : NinjectModule
    {
        public override void Load()
        {
            BindRepositories();

            ConfigurationStore configurationStore = GetAutomapperConfigurationStore();

            Bind<IConfiguration>().ToConstant(configurationStore);
            Bind<IMappingEngine>().ToConstant(GetAutomapperEngine(configurationStore));

            ConfigureAutomapperProfiles(configurationStore);

            BindRemoteServices();

            Bind<IHolidayResolver>().To<HolidayResolver>();
            Bind<IRouteSelector>().To<RouteSelector>();
            Bind<IAirportService>().To<AirportService>();
        }

        protected virtual void BindRepositories()
        {
            Bind<IEntityRepository<Airport>>().To<EntityRepository<Airport>>();
        }

        protected virtual void BindRemoteServices()
        {
            Bind<IFlightDataGetter>().To<AviagoScrapper>();
            Bind<ICityDataGetter>().To<WikitravelScrapper>();
        }

        private ConfigurationStore GetAutomapperConfigurationStore()
        {
            return new ConfigurationStore(new TypeMapFactory(), MapperRegistry.Mappers);
        }

        private MappingEngine GetAutomapperEngine(ConfigurationStore store)
        {
            return new MappingEngine(store,
                PlatformAdapter.Resolve<IDictionaryFactory>().CreateDictionary<TypePair, IObjectMapper>(),
                type => KernelInstance.Get(type));
        }

        private void ConfigureAutomapperProfiles(ConfigurationStore store)
        {
            store.AddProfile(new RawFlightDataProfile(KernelInstance.Get<IEntityRepository<Airport>>()));
        }
    }
}
