﻿using System.Data.Entity;
using System.Linq;
using Ninject.Activation;
using Ninject.Web.Common;
using Tripplanner.DataAccess.EntityDatabase;
using Tripplanner.DataAccess.HttpWrappers;

namespace Tripplanner.ServiceBindings.Production
{
    public class ProductionBindings: ServiceBindingsBase
    {
        public override void Load()
        {
            Bind<DbContext>().ToMethod(x => new TripplannerContext()).InScope(GetRequestScope);
            Bind<IHttpRequestFactory>().To<HttpRequestFactory>();
            base.Load();
        }

        private object GetRequestScope(IContext ctx)
        {
            return ctx.Kernel.Components
                .GetAll<INinjectHttpApplicationPlugin>()
                .Select(c => c.GetRequestScope(ctx))
                .FirstOrDefault(s => s != null);
        }
    }
}
