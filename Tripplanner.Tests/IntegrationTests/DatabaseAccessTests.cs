﻿using System.Linq;
using Ninject;
using NUnit.Framework;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;
using Tripplanner.ServiceBindings.Tests;

namespace Tripplanner.Tests.IntegrationTests
{
#if RELEASE
    [Ignore]
#endif
    [TestFixture]
    public class DatabaseAccessTests
    {
        private IKernel kernel;
        private IEntityRepository<Airport> airportRepository;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            kernel = new StandardKernel(new IntegrationTestsBindings());
            airportRepository = kernel.Get<IEntityRepository<Airport>>();
        }

        [Test]
        public void RepositoryShouldBeAbleToSaveAirport()
        {
            airportRepository.Add(new Airport
            {
                AirportCode = "SVO",
                City = "Moscow",
                Country = "Russia",
                FriendlyName = "Sheremetyevo International Airport",
                TimeZoneId = "E. Europe Standard Time"
            });
            airportRepository.SaveChanges();

            Assert.That(airportRepository.Get(x => x.AirportCode == "SVO").Count(), Is.EqualTo(1));
        }

        [Test]
        public void RepositoryShouldBeAbleToUpdateAirport()
        {
            Airport airport = airportRepository.Get(x => x.AirportCode == "LED").First();

            Assert.That(airport.FriendlyName, Is.EqualTo("Pulkovo Airport"));

            airport.FriendlyName = "Pulkovo";
            airportRepository.Update(airport);
            airportRepository.SaveChanges();

            Assert.That(airportRepository.Get(x => x.AirportCode == "LED").First().FriendlyName, Is.EqualTo("Pulkovo"));
        }

        [Test]
        public void RepositoryShouldBeAbleToDeleteAirport()
        {
            Airport airport = airportRepository.Get(x => x.AirportCode == "DME").First();

            Assert.That(airport, Is.Not.Null);

            airportRepository.Delete(airport);
            airportRepository.SaveChanges();

            Assert.That(airportRepository.Get(x => x.AirportCode == "DME").FirstOrDefault(), Is.Null);
        }

        [Test]
        public void RepositoryShouldBeAbleToGetAirportById()
        {
            Airport airport = airportRepository.Get(x => x.AirportCode == "LED").First();
            Airport anotherAirport = airportRepository.GetById(airport.Id);

            Assert.That(airport.AirportCode, Is.EqualTo(anotherAirport.AirportCode));
        }
    }
}
