﻿using Ninject;
using NUnit.Framework;
using Tripplanner.DataAccess.WikitravelScrapper;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Cities;
using Tripplanner.ServiceBindings.Tests;

namespace Tripplanner.Tests.IntegrationTests
{
#if RELEASE
    [Ignore]
#endif
    public class WikitravelScrapperTests
    {
        protected IKernel kernel;
       
        protected WikitravelScrapper wikitravelDataGetter;
        protected City cityInfo;

        private const string CityName = "Riga";

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            KernelSetup();
            CityDataGetterSetup();
            cityInfo = wikitravelDataGetter.GetCityInformation(CityName);
        }

        protected virtual void KernelSetup()
        {
            kernel = new StandardKernel(new IntegrationTestsBindings());
        }

        protected virtual void CityDataGetterSetup()
        {
            wikitravelDataGetter = kernel.Get<ICityDataGetter>() as WikitravelScrapper;
        }

        [Test]
        public void WikitravelScrapperShouldGetCityInfo()
        {
            Assert.That(cityInfo, Is.Not.Null);
        }

        [Test]
        public void CityInfoShouldContainAttractions()
        {
            Assert.That(cityInfo.Attractions, Is.Not.Null.And.Count.Not.EqualTo(0));
        }
    }
}
