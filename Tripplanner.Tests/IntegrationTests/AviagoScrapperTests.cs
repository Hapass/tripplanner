﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using NUnit.Framework;
using Tripplanner.DataAccess.AviagoScrapper;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;
using Tripplanner.ServiceBindings.Tests;

namespace Tripplanner.Tests.IntegrationTests
{
#if RELEASE
    [Ignore]
#endif
    [TestFixture]
    public class AviagoScrapperTests
    {
        protected IKernel kernel;
        protected IList<Flight> flightOptions;
        protected DateTime departureDate;

        protected AviagoScrapper flightDataGetter;

        private const string FromAirportCode = "MSQ";
        private const string ToAirportCode = "LED";

        public AviagoScrapperTests()
        {
            departureDate = DateTime.Now.Date.AddMonths(1);
        }

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            KernelSetup();
            flightDataGetter = kernel.Get<IFlightDataGetter>() as AviagoScrapper;
            flightOptions = flightDataGetter.GetFlightInformation(new FlightRequest
                {
                    DepartureDate = departureDate,
                    FromAirpotCode = FromAirportCode,
                    ToAirportCode = ToAirportCode
                });
        }

        protected virtual void KernelSetup()
        {
            kernel = new StandardKernel(new IntegrationTestsBindings());
        }

        [Test]
        public void AviagoScrapperShouldGetSomeFlightOptions()
        {
            Assert.That(flightOptions, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        public void FlightOptionsShouldContainFlightWithProperAirline()
        {
            Flight anyFlight = flightOptions.First();
            Assert.That(anyFlight.Airline, Is.Not.Null.And.Not.EqualTo(string.Empty));
        }

        [Test]
        public void FlightOptionsShouldContainFlightWithProperCost()
        {
            Flight anyFlight = flightOptions.First();
            Assert.That(anyFlight.Cost, Is.Not.EqualTo(0));
        }

        [Test]
        public void FlightOptionsShouldContainFlightWithProperDepartureAirportCode()
        {
            Flight anyFlight = flightOptions.First();
            Assert.That(anyFlight.DepartureAirportCode, Is.EqualTo(FromAirportCode));
        }

        [Test]
        public void FlightOptionsShouldContainFlightWithProperArrivalAirportCode()
        {
            Flight anyFlight = flightOptions.First();
            Assert.That(anyFlight.ArrivalAirportCode, Is.EqualTo(ToAirportCode));
        }

        [Test]
        public void FlightOptionsShouldContainFlightWithProperDepartureTime()
        {
            Flight anyFlight = flightOptions.First();
            Assert.That(anyFlight.DepartureTime.Date, Is.EqualTo(departureDate.Date));
            Assert.That(anyFlight.DepartureTime.Offset, Is.EqualTo(GetTimezoneOffsetForAirport(FromAirportCode, anyFlight.DepartureTime.DateTime)));
        }

        private TimeSpan GetTimezoneOffsetForAirport(string airportCode, DateTime dateTime)
        {
            IEntityRepository<Airport> airportRepository = kernel.Get<IEntityRepository<Airport>>();
            Airport fromAirport = airportRepository.Get(x => x.AirportCode == airportCode).First();
            TimeZoneInfo info = TimeZoneInfo.FindSystemTimeZoneById(fromAirport.TimeZoneId);
            return info.GetUtcOffset(dateTime);
        }

        [Test]
        public void FlightOptionsShouldContainFlightWithProperFlightTimeAndArrivalTime()
        {
            Flight anyFlight = flightOptions.First();
            Assert.That(anyFlight.DepartureTime + anyFlight.TimeInFlight, Is.EqualTo(anyFlight.ArrivalTime));
            Assert.That(anyFlight.ArrivalTime.Offset, Is.EqualTo(anyFlight.DepartureTime.Offset));
        }

        [Test]
        public void FlightOptionsShouldContainFlightWithProperLaggageWeightPerPerson()
        {
            Flight anyFlight = flightOptions.First();
            Assert.That(anyFlight.LaggageWeightPerPerson, Is.Not.EqualTo(0));
        }

        [Test]
        public void FlightOptionsShouldBeDistinct()
        {
            Assert.That(flightOptions.Distinct(new FlightComparer()).Count(), Is.EqualTo(flightOptions.Count));
        }

        private class FlightComparer : IEqualityComparer<Flight>
        {
            public bool Equals(Flight x, Flight y)
            {
                return x.DepartureTime == y.DepartureTime &&
                       x.DepartureAirportCode == y.DepartureAirportCode &&
                       x.Airline == y.Airline;
            }

            public int GetHashCode(Flight obj)
            {
                return (obj.Airline + 
                        obj.DepartureTime + 
                        obj.DepartureAirportCode).GetHashCode();
            }
        }
    }
}
