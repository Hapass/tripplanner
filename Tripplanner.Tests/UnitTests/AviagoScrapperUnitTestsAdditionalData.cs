﻿using Ninject;
using NUnit.Framework;
using Tripplanner.ServiceBindings.Tests;
using Tripplanner.Tests.IntegrationTests;

namespace Tripplanner.Tests.UnitTests
{
    /// <summary>
    /// The tests in this class are more particular, because we know the exact data that should be passed to html parser.
    /// The data could be found in App_Data/strange_case.html
    /// </summary>
    [TestFixture]
    public class AviagoScrapperUnitTestsAdditionalData : AviagoScrapperTests
    {
        protected override void KernelSetup()
        {
            kernel = new StandardKernel(new UnitTestsBindings("strange_case.html"));
        }
    }
}
