﻿using System;
using Ninject;
using NUnit.Framework;
using Tripplanner.ServiceBindings.Tests;
using Tripplanner.Tests.IntegrationTests;

namespace Tripplanner.Tests.UnitTests
{
    /// <summary>
    /// The tests in this class are more particular, because we know the exact data that should be passed to html parser.
    /// The data could be found in App_Data/aviago_data.html
    /// </summary>
    [TestFixture]
    public class AviagoScrapperUnitTests : AviagoScrapperTests
    {
        protected override void KernelSetup()
        {
            kernel = new StandardKernel(new UnitTestsBindings("aviago_data.html"));
        }

        [Test]
        public void FlightOptionsShouldCountShouldBeThree()
        {
            Assert.That(flightOptions.Count, Is.EqualTo(4));
        }

        [Test]
        public void AllFlightOptionsShouldContainTheAppropriatePrice()
        {
            Assert.That(flightOptions[0].Cost, Is.EqualTo(65.44m));
            Assert.That(flightOptions[1].Cost, Is.EqualTo(65.44m));
            Assert.That(flightOptions[2].Cost, Is.EqualTo(65.44m));
            Assert.That(flightOptions[3].Cost, Is.EqualTo(108.44m));
        }

        [Test]
        public void AllFlightOptionsShouldContainTheAppropriateAirline()
        {
            Assert.That(flightOptions[0].Airline, Is.EqualTo("Belavia"));
            Assert.That(flightOptions[1].Airline, Is.EqualTo("Belavia"));
            Assert.That(flightOptions[2].Airline, Is.EqualTo("Belavia"));
            Assert.That(flightOptions[3].Airline, Is.EqualTo("Belavia"));
        }

        [Test]
        public void AllFlightOptionsShouldContainTheAppropriateDepartureTime()
        {
            Assert.That(flightOptions[0].DepartureTime.DateTime, Is.EqualTo(departureDate.AddHours(22).AddMinutes(55)));
            Assert.That(flightOptions[1].DepartureTime.DateTime, Is.EqualTo(departureDate.AddHours(8).AddMinutes(35)));
            Assert.That(flightOptions[2].DepartureTime.DateTime, Is.EqualTo(departureDate.AddHours(12).AddMinutes(50)));
            Assert.That(flightOptions[3].DepartureTime.DateTime, Is.EqualTo(departureDate.AddHours(17).AddMinutes(35)));
        }

        [Test]
        public void AllFlightOptionsShouldContainTheAppropriateFlightTime()
        {
            Assert.That(flightOptions[0].TimeInFlight, Is.EqualTo(new TimeSpan(0, 1, 15, 0)));
            Assert.That(flightOptions[1].TimeInFlight, Is.EqualTo(new TimeSpan(0, 1, 15, 0)));
            Assert.That(flightOptions[2].TimeInFlight, Is.EqualTo(new TimeSpan(0, 1, 15, 0)));
            Assert.That(flightOptions[3].TimeInFlight, Is.EqualTo(new TimeSpan(0, 1, 15, 0)));
        }

        [Test]
        public void AllFlightOptionsShouldContainTheAppropriateArrivalTime()
        {
            Assert.That(flightOptions[0].ArrivalTime.DateTime, Is.EqualTo(flightOptions[0].DepartureTime.DateTime + flightOptions[0].TimeInFlight));
            Assert.That(flightOptions[1].ArrivalTime.DateTime, Is.EqualTo(flightOptions[1].DepartureTime.DateTime + flightOptions[1].TimeInFlight));
            Assert.That(flightOptions[2].ArrivalTime.DateTime, Is.EqualTo(flightOptions[2].DepartureTime.DateTime + flightOptions[2].TimeInFlight));
            Assert.That(flightOptions[3].ArrivalTime.DateTime, Is.EqualTo(flightOptions[3].DepartureTime.DateTime + flightOptions[3].TimeInFlight));
        }

        [Test]
        public void AllFlightOptionsShouldContainTheAppropriateLaggageWeightPerPerson()
        {
            Assert.That(flightOptions[0].LaggageWeightPerPerson, Is.EqualTo(20));
            Assert.That(flightOptions[1].LaggageWeightPerPerson, Is.EqualTo(20));
            Assert.That(flightOptions[2].LaggageWeightPerPerson, Is.EqualTo(20));
            Assert.That(flightOptions[3].LaggageWeightPerPerson, Is.EqualTo(20));
        }
    }
}
