﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Ninject;
using NUnit.Framework;
using Tripplanner.Controllers;
using Tripplanner.Logic.ServiceInterfaces;
using Tripplanner.Models;
using Tripplanner.ServiceBindings.Tests;

namespace Tripplanner.Tests.UnitTests
{
    [TestFixture]
    public class AirportControllerTests
    {
        private const string ErrorMessage = "ErrorMessage";

        private AirportController controller;
        private Mock<IAirportService> airportServiceMock;
        private IKernel kernel;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            kernel = new StandardKernel(new UnitTestsBindings());
            airportServiceMock = new Mock<IAirportService>();
            airportServiceMock.Setup(x => x.GetAllAirports()).Throws(new Exception(ErrorMessage));
        }

        [Test]
        public void SupportedCitiesActionShouldReturnAListOfDestinationCities()
        {
            controller = new AirportController(kernel.Get<IAirportService>());
            JsonResultModel<IEnumerable<string>> model = controller.SupportedCities().Data as JsonResultModel<IEnumerable<string>>;

            Assert.That(model, Is.Not.Null);
            Assert.That(model.Success, Is.True);
            Assert.That(model.ErrorMessage, Is.Null.Or.Empty);
            Assert.That(model.Data.Count(), Is.EqualTo(2));
        }

        [Test]
        public void SupportedCitiesActionShouldReturnErrorIfThrown()
        {
            controller = new AirportController(airportServiceMock.Object);
            JsonResultModel<object> model = controller.SupportedCities().Data as JsonResultModel<object>;

            Assert.That(model, Is.Not.Null);
            Assert.That(model.Success, Is.False);
            Assert.That(model.Data, Is.Null);
            Assert.That(model.ErrorMessage, Is.EqualTo(ErrorMessage));
        }
    }
}
