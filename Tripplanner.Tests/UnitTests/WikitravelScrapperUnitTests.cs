﻿using Ninject;
using NUnit.Framework;
using Tripplanner.ServiceBindings.Tests;
using Tripplanner.Tests.IntegrationTests;

namespace Tripplanner.Tests.UnitTests
{
    [TestFixture]
    class WikitravelScrapperUnitTests : WikitravelScrapperTests
    {
        protected override void KernelSetup()
        {
            kernel = new StandardKernel(new UnitTestsBindings("wikitravel_Riga.html"));
        }

        [Test]
        public void CityShouldContainCorrectAttractionsNumber()
        {
            Assert.That(cityInfo.Attractions.Count, Is.EqualTo(58));
        }

        [Test]
        public void CityInfoShouldContainCorrectAttractionsForRiga()
        {
            Assert.That(cityInfo.Attractions.Contains("Statue of Roland"), Is.True);
            Assert.That(cityInfo.Attractions.Contains("Clock"), Is.True);
            Assert.That(cityInfo.Attractions.Contains("Triangula Bastion"), Is.True);
            Assert.That(cityInfo.Attractions.Contains("Statue of Roland"), Is.True);
            Assert.That(cityInfo.Attractions.Contains("St. Peter's Church"), Is.True);
            Assert.That(cityInfo.Attractions.Contains("Museum of the Barricades of 1991"), Is.True);
            Assert.That(cityInfo.Attractions.Contains("St. Jacob's Barracks"), Is.True);
            Assert.That(cityInfo.Attractions.Contains("Latvian Photographic Museum"), Is.True);
        }
    }
}
