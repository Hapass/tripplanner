﻿using System;
using NUnit.Framework;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.Tests.UnitTests
{
    [TestFixture]
    public class FlightRequestTests
    {
        [Test]
        public void FlightRequestDepartureDateShouldHaveKindUnspecified()
        {
            FlightRequest flightRequest = new FlightRequest
            {
                DepartureDate = DateTime.Now.Date.AddMonths(1)
            };
            Assert.That(flightRequest.DepartureDate.Kind, Is.EqualTo(DateTimeKind.Unspecified));
        }
    }
}
