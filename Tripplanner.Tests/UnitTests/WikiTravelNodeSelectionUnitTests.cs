﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using NUnit.Framework;
using Tripplanner.DataAccess.WikitravelScrapper;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Cities;
using Tripplanner.ServiceBindings.Tests;

namespace Tripplanner.Tests.UnitTests
{
    [TestFixture]
    public class WikitravelNodeSelectionUnitTests
    {
        protected IKernel kernel;

        protected WikitravelScrapper wikitravelDataGetter;
        protected City cityInfo;

        private const string CityName = "Test";

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            KernelSetup();
            CityDataGetterSetup();
            cityInfo = wikitravelDataGetter.GetCityInformation(CityName);
        }

        protected void KernelSetup()
        {
            kernel = new StandardKernel(new UnitTestsBindings("wikitravel_with_diff_cases.html"));
        }

        protected void CityDataGetterSetup()
        {
            wikitravelDataGetter = kernel.Get<ICityDataGetter>() as WikitravelScrapper;
        }

        [Test]
        public void WikitravelScrapperShouldRecognizeFirstPBnode()
        {
            Assert.That(cityInfo.Attractions.Contains("p_b"), Is.True);
        }

        [Test]
        public void WikitravelScrapperShouldNotRecognizePbNodeWithWrongSelectorsBetween()
        {
            Assert.That(cityInfo.Attractions.Contains("p_wrong_selectors_b"), Is.False);
        }

        [Test]
        public void WikitravelScrapperShouldRecognizeFirstBoldInLiNode()
        {
            Assert.That(cityInfo.Attractions.Contains("li_b"), Is.True);
        }

        [Test]
        public void WikitravelScrapperShouldNotRecognizeBoldInLiNodeWithWrongSelectorsBetween()
        {
            Assert.That(cityInfo.Attractions.Contains("li_wrong_selectors_b"), Is.False);
        }

        [Test]
        public void WikitravelScrapperShouldRecognizeFirstBoldInRefNode()
        {
            Assert.That(cityInfo.Attractions.Contains("a_b"), Is.True);
        }

        [Test]
        public void WikitravelScrapperShouldNotRecognizeBoldInRefNodeWithWrongSelectorsBetween()
        {
            Assert.That(cityInfo.Attractions.Contains("a_wrong_selectors_b"), Is.False);
        }

        [Test]
        public void WikitravelScrapperShouldNotRecognizeNotFirstBoldNode()
        {
            Assert.That(cityInfo.Attractions.Contains("wrong_a_b"), Is.False);
            Assert.That(cityInfo.Attractions.Contains("wrong_li_b"), Is.False);
            Assert.That(cityInfo.Attractions.Contains("wrong_a_b"), Is.False);
        }

        [Test]
        public void WikitravelScrapperShouldRecognizeSpanNodeWithConcreteClass()
        {
            Assert.That(cityInfo.Attractions.Contains("span_fn_org"), Is.True);
        }
    }
}
