﻿using NUnit.Framework;
using Tripplanner.Controllers;
using Tripplanner.Models;
using Tripplanner.StringStore;

namespace Tripplanner.Tests.UnitTests
{
    [TestFixture]
    public class HomeControllerTests
    {
        private HomeController controller;
        private IndexModel model;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            controller = new HomeController();
            model = controller.Index().Model as IndexModel;
        }

        [Test]
        public void IndexActionShouldReturnResult()
        {
            Assert.That(model, Is.Not.Null);
            Assert.That(model.PageTitle, Is.EqualTo(Resources.Index_Title));
        }

        [Test]
        public void IndexActionShouldReturnAppropriateErrorMessages()
        {
            Assert.That(model, Is.Not.Null);
            Assert.That(model.ErrorMessages["required"], Is.EqualTo(Resources.Validation_Required));
            Assert.That(model.ErrorMessages["maxlength"], Is.EqualTo(Resources.Validation_MaxLength));
            Assert.That(model.ErrorMessages["xss"], Is.EqualTo(Resources.Validation_Xss));
        }
    }
}
