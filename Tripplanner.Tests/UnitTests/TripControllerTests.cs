﻿using System;
using Ninject;
using NUnit.Framework;
using Tripplanner.Controllers;
using Tripplanner.Logic.Domain.Flights;
using Tripplanner.Logic.ServiceInterfaces;
using Tripplanner.Models;
using Tripplanner.ServiceBindings.Tests;

namespace Tripplanner.Tests.UnitTests
{
    [TestFixture]
    public class TripControllerTests
    {
        private TripController controller;
        private IAirportService airportService;
        private IHolidayResolver holidayResolver;
        private DateTime departureDate;

        [SetUp]
        public void SetUp()
        {
            IKernel kernel = new StandardKernel(new UnitTestsBindings("aviago_data.html"));
            controller = new TripController(kernel.Get<IRouteSelector>());
            airportService = kernel.Get<IAirportService>();
            holidayResolver = kernel.Get<IHolidayResolver>();
            departureDate = holidayResolver.GetClosestSaturday(airportService.GetAirportByCity("Minsk").TimeZoneId);
        }

        [Test]
        public void FindActionShouldReturnOptimalRoute()
        {
            JsonResultModel<Flight> model = controller.Find(new TripRequirementsModel
            {
                DestinationPoint = "Saint Petersburg"
            }).Data as JsonResultModel<Flight>;

            Assert.That(model, Is.Not.Null);
            Assert.That(model.ErrorMessage, Is.Null.Or.Empty);
            Assert.That(model.Success, Is.True);
            Assert.That(model.Data.Airline, Is.EqualTo("Belavia"));
            Assert.That(model.Data.Cost, Is.EqualTo(65.44m));
            Assert.That(model.Data.DepartureTime.DateTime, Is.EqualTo(departureDate.AddHours(8).AddMinutes(35)));
        }

        [Test]
        public void FindActionShouldReturnAppropriateErrorMessages()
        {
            JsonResultModel<object> model = controller.Find(new TripRequirementsModel
            {
                DestinationPoint = "Brusnika"
            }).Data as JsonResultModel<object>;

            Assert.That(model, Is.Not.Null);
            Assert.That(model.ErrorMessage, Is.Not.Null.And.Not.Empty);
            Assert.That(model.Data, Is.Null);
        }

        [Test]
        public void FindActionShouldReturnErrorMessageIfModelContainsInvalidData()
        {
            controller.ModelState.AddModelError("Error1", "Error1Message");
            controller.ModelState.AddModelError("Error2", "Error2Message");
            JsonResultModel<object> model = controller.Find(new TripRequirementsModel()).Data as JsonResultModel<object>;

            Assert.That(model, Is.Not.Null);
            Assert.That(model.ErrorMessage, Is.EqualTo("Error1Message"));
            Assert.That(model.Data, Is.Null);
        }
    }
}
