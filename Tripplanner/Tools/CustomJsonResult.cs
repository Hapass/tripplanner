﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Tripplanner.Models;

namespace Tripplanner.Tools
{
    public class CustomJsonResult : JsonResult
    {
        private const string JsonContentType = "application/json";
        private const string HtmlContentType = "text/html";
        private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss";

        private CustomJsonResult()
        {
            JsonRequestBehavior = JsonRequestBehavior.AllowGet;
        }

        public static CustomJsonResult TryGetJsonResult<T>(Func<T> function)
        {
            CustomJsonResult result = new CustomJsonResult();
            try
            {
                result.SetData(function());
            }
            catch (Exception e)
            {
                result.SetError(e.Message);
            }
            return result;
        }

        public static CustomJsonResult GetErrorResult(ModelStateDictionary modelState)
        {
            if (modelState.IsValid)
            {
                throw new ArgumentException("You should call this method only when your model is invalid.");
            }
            KeyValuePair<string, ModelState> pair = modelState.First();
            return new CustomJsonResult().SetError(pair.Value.Errors.First().ErrorMessage);
        }

        public override void ExecuteResult(ControllerContext context)
        {
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = GetContentType(context);
            response.ContentEncoding = ContentEncoding ?? response.ContentEncoding;
            response.Write(JsonConvert.SerializeObject(Data, GetSettings()));
        }

        private string GetContentType(ControllerContext context)
        {
            return context.HttpContext.Request.Headers["Accept"].Contains(JsonContentType)
                ? JsonContentType : HtmlContentType;
        }

        private JsonSerializerSettings GetSettings()
        {
            return new JsonSerializerSettings
            {
                StringEscapeHandling = StringEscapeHandling.EscapeHtml,
                DateFormatString = DateTimeFormat,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }

        public CustomJsonResult SetData<T>(T data)
        {
            Data = new JsonResultModel<T>
            {
                Data = data
            };
            return this;
        }

        public CustomJsonResult SetError(string error)
        {
            Data = new JsonResultModel<object>
            {
                ErrorMessage = error
            };
            return this;
        }
    }
}