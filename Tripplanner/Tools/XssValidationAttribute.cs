﻿using System;
using System.ComponentModel.DataAnnotations;
using Tripplanner.StringStore;

namespace Tripplanner.Tools
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class XssValidationAttribute : RegularExpressionAttribute
    {
        public XssValidationAttribute() : base(@"^((?!&#|<[a-zA-Z!\?/]+)[\s\S])*$")
        {
            ErrorMessageResourceName = "Validation_Xss";
            ErrorMessageResourceType = typeof (Resources);
        }
    }
}