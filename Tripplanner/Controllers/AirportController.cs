﻿using System.Linq;
using System.Web.Mvc;
using Tripplanner.Logic.ServiceInterfaces;
using Tripplanner.Tools;

namespace Tripplanner.Controllers
{
    public class AirportController : Controller
    {
        private readonly IAirportService airportService;

        public AirportController(IAirportService airportService)
        {
            this.airportService = airportService;
        }

        [HttpGet]
        public JsonResult SupportedCities()
        {
            return CustomJsonResult.TryGetJsonResult(() => airportService.GetAllAirports().Select(x => x.City).Distinct());
        }
    }
}