﻿using System.Web.Mvc;
using Tripplanner.Logic.ServiceInterfaces;
using Tripplanner.Models;
using Tripplanner.Tools;

namespace Tripplanner.Controllers
{
    public class TripController : Controller
    {
        private readonly IRouteSelector routeSelector;

        public TripController(IRouteSelector routeSelector)
        {
            this.routeSelector = routeSelector;
        }

        [HttpPost]
        public JsonResult Find(TripRequirementsModel tripRequirements)
        {
            return !ModelState.IsValid ?
                CustomJsonResult.GetErrorResult(ModelState) :
                CustomJsonResult.TryGetJsonResult(() => routeSelector.SelectRoute(tripRequirements.DestinationPoint));
        }
    }
}