﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Tripplanner.Models;
using Tripplanner.StringStore;

namespace Tripplanner.Controllers
{
    public class HomeController : Controller
    {
        private const string ValidationMessagePrefix = "Validation_";

        [HttpGet]
        public ViewResult Index()
        {
            IndexModel model = new IndexModel
            {
                ErrorMessages = GetAllValidationMessages(),
                PageTitle = Resources.Index_Title
            };
            return View(model);
        }

        private IDictionary<string, string> GetAllValidationMessages()
        {
            IDictionary<string, string> result = new Dictionary<string, string>();
            foreach (PropertyInfo property in GetValidationMessageProperties())
            {
                result.Add(property.Name.Substring(ValidationMessagePrefix.Length)
                    .ToLower(CultureInfo.InvariantCulture), (string)property.GetValue(null));
            }
            return result;
        }

        private IEnumerable<PropertyInfo> GetValidationMessageProperties()
        {
            Type resourcesType = typeof(Resources);
            return resourcesType
                .GetRuntimeProperties()
                .Where(p => p.Name.StartsWith(ValidationMessagePrefix));
        }
    }
}