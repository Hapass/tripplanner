﻿angular.module("customForms", [])

.directive("xss", [function() {
    return {
        require: "ngModel",
        link: function(scope, element, attr, ngModelController) {
            ngModelController.$validators.xss = function(modelValue, viewValue) {
                if (ngModelController.$isEmpty(modelValue)) {
                    return true;
                }

                if (/^((?!&#|<[a-zA-Z!\?/]+)[\s\S])*$/.test(viewValue)) {
                    return true;
                }

                return false;
            }
        }
    };
}])

.directive("inputWrapper", [function() {
    return {
        transclude: true,
        templateUrl: "Client/application/html/input-wrapper.html",
        scope: {
            label: "@",
            inputReference: "="
        }
    };
}]);