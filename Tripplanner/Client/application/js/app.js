﻿angular.module("app", ["ui.bootstrap", "ngRoute", "ngMessages", "routeSelector", "customForms"])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when("/index", {
        templateUrl: "Client/application/html/main-form.html",
        controller: "RouteRequirementsController"
    }).when("/trip", {
        templateUrl: "Client/application/html/route-results.html",
        controller: "RouteResultsController"
    }).otherwise({
        redirectTo: "/index"
    });
}]);

