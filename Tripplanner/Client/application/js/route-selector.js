﻿angular.module("routeSelector", [])

.controller("RouteRequirementsController", ["$scope", "$location", "tripplannerService", "tripDataService", function ($scope, $location, tripplannerService, tripDataService) {
    $scope.model = {};
    $scope.cities = [];

    $scope.planTrip = function (tripRequirementsModel) {
        if ($scope.tripRequirements.$invalid || $scope.isLoading) {
            return;
        }

        $scope.isLoading = true;
        tripplannerService.planTrip(tripRequirementsModel).then(resolveTripData);

        function resolveTripData(response) {
            if (!response.data.success) {
                return;
            }

            tripDataService.saveData(response.data.data);
            $location.path("/trip");
        }
    }

    $scope.$on("$routeChangeSuccess", function(){
        $scope.isLoading = false;
    });

    tripplannerService.getSupportedCities().then(function (result) {
        $scope.cities = result.data.data;
    });
}])

.controller("RouteResultsController", ["$scope", "tripDataService", function($scope, tripDataService) {
    $scope.model = tripDataService.getData();
}])

.factory("tripplannerService", ["$http", function($http) {
    return {
        planTrip: function(tripRequirementsModel) {
            return $http.post("/Trip/Find", tripRequirementsModel);
        },

        getSupportedCities: function() {
            return $http.get("/Airport/SupportedCities");
        }
    };
}])

.factory("tripDataService", [function () {
    var storedData = {};
    return {
        saveData: function(data) {
            storedData = data;
        },
        getData: function() {
            return storedData;
        }
    };
}]);