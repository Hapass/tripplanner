﻿namespace Tripplanner.Models
{
    public class JsonResultModel<T>
    {
        public T Data { get; set; }

        public bool Success
        {
            get { return Data != null && string.IsNullOrEmpty(ErrorMessage); }
        }

        public string ErrorMessage { get; set; }
    }
}