﻿using System.ComponentModel.DataAnnotations;
using Tripplanner.StringStore;
using Tripplanner.Tools;

namespace Tripplanner.Models
{
    public class TripRequirementsModel
    {
        [Required(ErrorMessageResourceName = "Validation_Required", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(50, ErrorMessageResourceName = "Validation_MaxLength", ErrorMessageResourceType = typeof(Resources))]
        [XssValidation]
        public string DestinationPoint { get; set; }
    }
}