﻿using System.Collections.Generic;

namespace Tripplanner.Models
{
    public class IndexModel
    {
        public string PageTitle { get; set; }
        public IDictionary<string, string> ErrorMessages { get; set; }
    }
}