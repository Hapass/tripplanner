﻿using System.Web.Optimization;

namespace Tripplanner.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts")
                .Include("~/Client/components/angular/angular.js",
                         "~/Client/components/angular-bootstrap/ui-bootstrap-tpls.js",
                         "~/Client/components/angular-route/angular-route.js",
                         "~/Client/components/angular-messages/angular-messages.js",
                         "~/Client/application/js/app.js",
                         "~/Client/application/js/custom-forms.js",
                         "~/Client/application/js/route-selector.js"));

            bundles.Add(new StyleBundle("~/bundles/styles")
                .Include("~/Client/application/less/site.less"));
        }
    }
}