﻿using System.Collections.Generic;
using System.Linq;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;
using Tripplanner.Logic.ServiceInterfaces;

namespace Tripplanner.Logic.Services
{
    public class AirportService : IAirportService
    {
        private readonly IEntityRepository<Airport> airportRepository;

        public AirportService(IEntityRepository<Airport> airportRepository)
        {
            this.airportRepository = airportRepository;
        }

        public IEnumerable<Airport> GetAllAirports()
        {
            return airportRepository.Get(x => true);
        }

        public Airport GetAirportByCity(string city)
        {
            return airportRepository.Get(x => x.City == city).First();
        }
    }
}
