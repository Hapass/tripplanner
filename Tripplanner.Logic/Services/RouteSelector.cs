﻿using System.Collections.Generic;
using System.Linq;
using MoreLinq;
using Tripplanner.Logic.DataAccessInterfaces;
using Tripplanner.Logic.Domain.Flights;
using Tripplanner.Logic.ServiceInterfaces;

namespace Tripplanner.Logic.Services
{
    public class RouteSelector : IRouteSelector
    {
        private const string DefaultDepartureCity = "Minsk";

        private readonly IFlightDataGetter flightDataGetter;
        private readonly IAirportService airportService;
        private readonly IHolidayResolver holidayResolver;

        public RouteSelector(IFlightDataGetter flightDataGetter,
            IAirportService airportService,
            IHolidayResolver holidayResolver)
        {
            this.flightDataGetter = flightDataGetter;
            this.airportService = airportService;
            this.holidayResolver = holidayResolver;
        }

        /// <summary>
        /// Creates basic flight request. Defaults departure date to closest saturday. From airport defaults to Minsk National Airport.
        /// </summary>
        /// <param name="destinationPoint">Destination city name.</param>
        /// <returns>Best flight option.</returns>
        public Flight SelectRoute(string destinationPoint)
        {
            FlightRequest request = CreateRequest(destinationPoint);
            IEnumerable<Flight> availableFlights = flightDataGetter.GetFlightInformation(request);
            decimal minCost = availableFlights.MinBy(x => x.Cost).Cost;
            return availableFlights.Where(x => x.Cost == minCost).MinBy(x => x.ArrivalTime);
        }

        private FlightRequest CreateRequest(string destinationPoint)
        {
            Airport departureAirport = airportService.GetAirportByCity(DefaultDepartureCity);
            Airport destinationAirport = airportService.GetAirportByCity(destinationPoint);
            return new FlightRequest
            {
                DepartureDate = holidayResolver.GetClosestSaturday(departureAirport.TimeZoneId),
                FromAirpotCode = departureAirport.AirportCode,
                ToAirportCode = destinationAirport.AirportCode
            };
        }
    }
}
