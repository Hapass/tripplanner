﻿using System;
using Tripplanner.Logic.ServiceInterfaces;

namespace Tripplanner.Logic.Services
{
    public class HolidayResolver : IHolidayResolver
    {
        public DateTime GetClosestSaturday(string timeZoneId)
        {
            return GetClosestSaturday(DateTimeOffset.UtcNow, timeZoneId);
        }

        public DateTime GetClosestSaturday(DateTimeOffset now, string timeZoneId)
        {
            TimeZoneInfo info = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            DateTime dateInSpecificTimeZone = TimeZoneInfo.ConvertTimeFromUtc(now.UtcDateTime, info).Date;
            int daysToClosestSaturday = (int)DayOfWeek.Saturday - (int)dateInSpecificTimeZone.DayOfWeek;
            return dateInSpecificTimeZone.AddDays(daysToClosestSaturday);
        }
    }
}
