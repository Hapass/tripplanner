﻿using System.Collections.Generic;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.Logic.ServiceInterfaces
{
    public interface IAirportService
    {
        IEnumerable<Airport> GetAllAirports();
        Airport GetAirportByCity(string city);
    }
}
