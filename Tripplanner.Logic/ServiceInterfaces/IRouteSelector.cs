﻿using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.Logic.ServiceInterfaces
{
    public interface IRouteSelector
    {
        Flight SelectRoute(string destinationPoint);
    }
}
