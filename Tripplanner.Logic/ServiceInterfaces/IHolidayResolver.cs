﻿using System;

namespace Tripplanner.Logic.ServiceInterfaces
{
    public interface IHolidayResolver
    {
        DateTime GetClosestSaturday(string timeZoneId);
        DateTime GetClosestSaturday(DateTimeOffset now, string timeZoneId);
    }
}
