﻿namespace Tripplanner.Logic.Domain
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}
