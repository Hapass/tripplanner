﻿namespace Tripplanner.Logic.Domain.Flights
{
    public class Airport : Entity
    {
        public string AirportCode { get; set; }

        public string TimeZoneId { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string FriendlyName { get; set; }
    }
}
