﻿using System;

namespace Tripplanner.Logic.Domain.Flights
{
    public class Flight
    {
        public string Airline { get; set; }

        public DateTimeOffset DepartureTime { get; set; }
        public DateTimeOffset ArrivalTime { get; set; }

        public string DepartureAirportCode { get; set; }
        public string ArrivalAirportCode { get; set; }

        public TimeSpan TimeInFlight { get; set; }

        /// <summary>
        /// Cost in euro.
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Weight in kilograms.
        /// </summary>
        public int LaggageWeightPerPerson { get; set; }
    }
}
