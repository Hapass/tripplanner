﻿using System;

namespace Tripplanner.Logic.Domain.Flights
{
    public class FlightRequest
    {
        private DateTime departureDate;

        public string FromAirpotCode { get; set; }
        public string ToAirportCode { get; set; }

        /// <summary>
        /// Departure date in from airport timezone.
        /// </summary>
        public DateTime DepartureDate
        {
            get { return departureDate; }
            set
            {
                departureDate = DateTime.SpecifyKind(value, DateTimeKind.Unspecified);
            }
        }
    }
}
