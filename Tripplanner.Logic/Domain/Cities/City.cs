﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tripplanner.Logic.Domain.Cities
{
    public class City
    {
        public string Name { get; set; }
        public List<string> Attractions { get; set; }
    }
}
