﻿using System.Collections.Generic;
using Tripplanner.Logic.Domain.Flights;

namespace Tripplanner.Logic.DataAccessInterfaces
{
    public interface IFlightDataGetter
    {
        IList<Flight> GetFlightInformation(FlightRequest request);
    }
}
