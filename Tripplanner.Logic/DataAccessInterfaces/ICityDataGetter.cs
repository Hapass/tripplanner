﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tripplanner.Logic.Domain.Cities;

namespace Tripplanner.Logic.DataAccessInterfaces
{
    public interface ICityDataGetter
    {
        City GetCityInformation(string city);
    }
}
