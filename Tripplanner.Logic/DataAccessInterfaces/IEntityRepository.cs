﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Tripplanner.Logic.Domain;

namespace Tripplanner.Logic.DataAccessInterfaces
{
    public interface IEntityRepository<TEntity> where TEntity : Entity
    {
        TEntity GetById(int id);

        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> query);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        void SaveChanges();
    }
}
